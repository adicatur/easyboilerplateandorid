package example.easyboilerplateandroid.ui.activity;

import android.app.ProgressDialog;
import android.easyboilerplateandroid.R;
import example.easyboilerplateandroid.data.pojo.Account;
import example.easyboilerplateandroid.ui.presenter.MainPresenter;
import example.easyboilerplateandroid.ui.presenter.MvpView.MainMvp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainMvp {

    private MainPresenter mainPresenter;
    private ProgressDialog progressDialog;

    @BindView(R.id.text_email) TextView email;
    @BindView(R.id.text_token) TextView token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mainPresenter = new MainPresenter();

        mainPresenter.onAttachView(this);
        mainPresenter.loadData("p@test.com", "12345678");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainPresenter.onDetachView();
    }


    @Override
    public void showLoading() {
        if (progressDialog == null){
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
        }
        progressDialog.show();
    }

    @Override
    public void dismissLoading() {
        if (progressDialog != null){
            progressDialog.dismiss();
        }
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showData(Account data) {
        email.setText(data.getEmail());
        token.setText(data.getAuthenticationToken());
    }
}
