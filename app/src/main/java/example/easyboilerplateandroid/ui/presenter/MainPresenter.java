package example.easyboilerplateandroid.ui.presenter;

import example.easyboilerplateandroid.data.pojo.Account;
import example.easyboilerplateandroid.data.remote.EasyBoilerplateApi;
import example.easyboilerplateandroid.ui.base.BasePresenter;
import example.easyboilerplateandroid.ui.presenter.MvpView.MainMvp;
import android.util.Log;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by adicatur on 12/24/16.
 */

public class MainPresenter extends BasePresenter<MainMvp> {

    private Subscription subscription;

    @Override
    public void onAttachView(MainMvp view) {
        super.onAttachView(view);
    }

    @Override
    public void onDetachView() {
        super.onDetachView();
        if(subscription != null){
            subscription.unsubscribe();
        }
    }

    public void loadData(String email, String password){
        checkViewAttached();
        getMvpView().showLoading();
        subscription = EasyBoilerplateApi.getInstance()
                .loadAccount(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Account>() {
                    @Override
                    public void call(Account account) {
                        Log.d("ctr", "call: ctr " + account.getEmail());
                        getMvpView().showData(account);
                        getMvpView().dismissLoading();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        getMvpView().showError(throwable.getMessage());
                        getMvpView().dismissLoading();
                    }
                });
    }
}
