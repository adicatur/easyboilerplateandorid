package example.easyboilerplateandroid.ui.presenter.MvpView;

import example.easyboilerplateandroid.data.pojo.Account;
import example.easyboilerplateandroid.ui.base.Mvp;

/**
 * Created by adicatur on 12/24/16.
 */

public interface MainMvp extends Mvp {

    void showData(Account data);

}
