package example.easyboilerplateandroid;

import android.app.Application;
import android.easyboilerplateandroid.BuildConfig;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;


public class EasyBoilerplateApplication extends Application {

    private static EasyBoilerplateApplication INSTACE;

    public static EasyBoilerplateApplication getInstace(){
           return INSTACE;
       }

    @Override
    public void onCreate() {
        super.onCreate();

        if(BuildConfig.DEBUG){
            Timber.plant(new Timber.DebugTree());
        }

        INSTACE = this;
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }
}
