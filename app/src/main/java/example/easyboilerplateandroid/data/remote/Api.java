package example.easyboilerplateandroid.data.remote;

import retrofit2.http.Field;
import rx.Observable;
import com.google.gson.JsonElement;

import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by adicatur on 12/24/16.
 */

public interface Api {

    @FormUrlEncoded
    @POST("/user/sign_in")
    Observable<JsonElement> login(@Field("email") String email,
                                  @Field("password") String password);


}
