package example.easyboilerplateandroid.data.remote;

import example.easyboilerplateandroid.data.pojo.Account;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by adicatur on 12/24/16.
 */

public enum  EasyBoilerplateApi {

    INSTANCE;
    private final String baseUrl = "https://e-parking.herokuapp.com";
    private Api api;
    private OkHttpClient httpClient;

    EasyBoilerplateApi() {

        httpClient = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        api = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(Api.class);
    }

    public static EasyBoilerplateApi getInstance(){
        return INSTANCE;
    }

    public Observable<Account> loadAccount(String email, String password){
        return api.login(email, password)
                .map(new Func1<JsonElement, Account>() {
                    @Override
                    public Account call(JsonElement jsonElement) {
                        Log.d("ctr", "call: ctr " + jsonElement.toString());
                        JsonObject jsonAccount = jsonElement.getAsJsonObject().get("data").getAsJsonObject()
                            .get("customer").getAsJsonObject();

                    Account account = new Account();
                    account.setAuthenticationToken(jsonAccount.get("authentication_token").getAsString());
                    account.setEmail(jsonAccount.get("email").getAsString());

                    return account;
                    }
                });
    }
}
