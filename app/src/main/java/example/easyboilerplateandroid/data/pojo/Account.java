package example.easyboilerplateandroid.data.pojo;

/**
 * Created by adicatur on 12/24/16.
 */

public class Account {
    private String authenticationToken;
    private String email;

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public String getEmail() {
        return email;
    }
}
